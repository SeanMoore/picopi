#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

HOSTNAME_NEW=picopi

echo 'Set up hostname.'
echo "$HOSTNAME_NEW" | sudo tee /etc/hostname
sudo sed -i -e "s/$HOSTNAME/$HOSTNAME_NEW/" /etc/hosts
