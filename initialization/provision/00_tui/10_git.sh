#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Install git.'
sudo apt-get install -y git