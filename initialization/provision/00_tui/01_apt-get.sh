#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Update apt.'
sudo apt-get update -y

# These can't really be run unattended without potentially causing problems.
# sudo apt-get upgrade -y
# sudo apt-get dist-upgrade -y
