#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

function run_script() {
    script=$1
    echo "START $script"
    cat "$script" | sudo -i -u pi bash
    echo "END $script"
}

function run_directory() {
    strap_dir=$1
    echo "START $strap_dir"
    for script in $(ls $strap_dir/*.sh | sort)
    do
        run_script "$script"
    done
    echo "END $strap_dir"
}

function run_provision() {
    run_directory "$1" | tee -a "$2/provision.log"
}

run_provision "$1" "."