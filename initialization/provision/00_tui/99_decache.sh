#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "Remove apt caching."
sudo rm /etc/apt/apt.conf.d/00proxy