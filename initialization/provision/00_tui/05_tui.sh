#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo 'Set up shell initialization.'
touch $HOME/.myrc

echo 'Set up bash initialization.'
echo "source ~/.myrc" >> $HOME/.bashrc

echo 'Set up profile initialization.'
echo "source ~/.myrc" >> $HOME/.profile
