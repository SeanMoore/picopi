# Pico Pi

See Chapter 6 "Debugging with SWD" of [Getting started with Raspberry Pi Pico](https://datasheets.raspberrypi.org/pico/getting-started-with-pico.pdf). All the tools should be preinstalled after generating the disk image.

```bash
./picopi generate
```

## Build Examples

```
cd /tmp/
mkdir pico-examples
cd pico-examples/
cmake /home/pi/pico/pico-examples/
make -j $(nproc)
```

## Programming Without Debugging

```
cd /tmp/pico_hello/build
OPENOCD=/home/pi/pico/openocd/tcl
openocd -f ${OPENOCD}/interface/raspberrypi-swd.cfg -f ${OPENOCD}/target/rp2040.cfg -c "program test.elf verify reset exit"
```

## Programming With Debugging

Plug in the Raspberry Pi Pico into the Raspberry Pi using both the SWD interface and the USB interface.

In a terminal start OpenOCD

```
cd /home/pi/pico/openocd
openocd -f interface/raspberrypi-swd.cfg -f target/rp2040.cfg
```
In another terminal start GDB

```
cd /tmp/pico-examples/hello_world/usb
gdb-multiarch hello_usb.elf
```

and within GDB connect to OpenOCD, load the program and then reset

```
target remote localhost:3333
load
monitor reset init
continue
```

## Check Operation

```
pi@picopi:/tmp/pico-examples/hello_world/usb $ cat /dev/ttyACM0
Hello, world!

Hello, world!

Hello, world!

Hello, world!

Hello, world!

^C
pi@picopi:/tmp/pico-examples/hello_world/usb $
```

